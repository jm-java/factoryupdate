class Product {
    private String name;
    private Integer weight;
    private Double price;

    public Product(String name, int weight, double price) {
        this.name = name;
        this.weight = weight;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Продукт - " +
                "наименование='" + name + '\'' +
                ", вес=" + weight +
                ", цена=" + price;
    }
}