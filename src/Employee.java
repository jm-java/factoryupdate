class Employee extends Worker{
    private String name;
    private Product product;

    public Employee(String name, Product product) {
        super(name);
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Рабочий - " +
                "ФИО='" + name + '\'' +
                ", Продукт=" + product;
    }
}