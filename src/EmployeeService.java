import java.util.Map;

public interface EmployeeService {
    void initializeEmployees();
    void hireEmployee(Employee employee);
    void fireEmployee(String name);
    Employee findEmployeeByName(String name);
    Map<String, Employee> getEmployees();
}


