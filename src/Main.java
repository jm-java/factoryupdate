public class Main {
    public static void main(String[] args) {
        EmployeeServiceImpl employeeServiceImpl = new EmployeeServiceImpl();
        ProductServiceImpl productServiceImpl = new ProductServiceImpl();

        employeeServiceImpl.initializeEmployees();
        productServiceImpl.initializeProducts();

        Menus menus = new Menus(productServiceImpl, employeeServiceImpl);
        menus.run();
    }
}