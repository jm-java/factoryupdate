import java.util.ArrayList;
import java.util.List;

class Manager extends Worker {
    private List<Worker> employees;

    public Manager(String name) {
        super(name);
        this.employees = new ArrayList<>();
    }

    public void addEmployee(Worker worker) {
        employees.add(worker);
    }

    public void removeEmployee(Worker worker) {
        employees.remove(worker);
    }

    @Override
    public String toString() {
        return "Менеджер - " +
                "ФИО='" + getName() + '\'' +
                ", Сотрудники=" + employees;
    }


}