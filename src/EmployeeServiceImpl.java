import java.util.HashMap;
import java.util.Map;

class EmployeeServiceImpl implements EmployeeService {
    private Map<String, Employee> employees;

    public EmployeeServiceImpl() {
        employees = new HashMap<>();
    }

    public void initializeEmployees() {
        Product product1 = new Product("Продукт 1", 100, 50D);
        Product product2 = new Product("Продукт 2", 150, 70D);
        Product product3 = new Product("Продукт 3", 200, 100D);

        Employee employee1 = new Employee("Иванов", product1);
        Employee employee2 = new Employee("Петров", product2);
        Employee employee3 = new Employee("Сидоров", product3);

        employees.put(employee1.getName(), employee1);
        employees.put(employee2.getName(), employee2);
        employees.put(employee3.getName(), employee3);
    }

    public void hireEmployee(Employee employee) {
        employees.put(employee.getName(), employee);
    }

    public void fireEmployee(String name) {
        employees.remove(name);
    }

    public Employee findEmployeeByName(String name) {
        return employees.get(name);
    }

    public Map<String, Employee> getEmployees() {
        return employees;
    }
}