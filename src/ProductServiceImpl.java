import java.util.Arrays;

class ProductServiceImpl implements ProductService {
    private Product[] products;

    public ProductServiceImpl() {
        products = new Product[0];
    }

    public void initializeProducts() {
        Product[] initialProducts = {
                new Product("Продукт 1", 100, 50),
                new Product("Продукт 2", 150, 70),
                new Product("Продукт 3", 200, 100),
                new Product("Продукт 4", 180, 90),
                new Product("Продукт 5", 210, 120)
        };
        products = initialProducts;
    }

    public int calculateTotalWeight() {
        int totalWeight = 0;
        for (Product product : products) {
            totalWeight += product.getWeight();
        }
        return totalWeight;
    }

    public double calculateAveragePrice() {
        double totalSum = 0;
        for (Product product : products) {
            totalSum += product.getPrice();
        }
        return totalSum / products.length;
    }

    public Product findOptimalProduct() {
        double minPricePerWeight = Double.MAX_VALUE;
        Product optimalProduct = null;
        for (Product product : products) {
            double pricePerWeight = product.getPrice() / product.getWeight();
            if (pricePerWeight < minPricePerWeight) {
                minPricePerWeight = pricePerWeight;
                optimalProduct = product;
            }
        }
        return optimalProduct;
    }

    public Product findProductByName(String name) {
        return Arrays.stream(products)
                .filter(product -> product.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    public void addProduct(Product product) {
        Product[] newProducts = new Product[products.length + 1];
        System.arraycopy(products, 0, newProducts, 0, products.length);
        newProducts[products.length] = product;
        products = newProducts;
    }

    public void updateProduct(Product product) {
        for (int i = 0; i < products.length; i++) {
            if (products[i].getName().equals(product.getName())) {
                products[i] = product;
                break;
            }
        }
    }

    public void deleteProduct(Product product) {
        Product[] newProducts = new Product[products.length - 1];
        int index = 0;
        for (Product p : products) {
            if (!p.getName().equals(product.getName())) {
                newProducts[index++] = p;
            }
        }
        products = newProducts;
    }

    public Product[] getProducts() {
        return products;
    }
}
