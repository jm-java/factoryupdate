public interface ProductService {
    Product findProductByName(String name);
    void addProduct(Product product);
    void updateProduct(Product product);
    void deleteProduct(Product product);
    Product[] getProducts();
    double calculateAveragePrice();
    void initializeProducts();
    int calculateTotalWeight();
    Product findOptimalProduct();
}
