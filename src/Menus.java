import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

class Menus {
    private final Scanner scanner;
    private final ProductServiceImpl productServiceImpl;
    private final EmployeeServiceImpl employeeServiceImpl;

    public Menus(ProductServiceImpl productServiceImpl, EmployeeServiceImpl employeeServiceImpl) {
        this.scanner = new Scanner(System.in);
        this.productServiceImpl = productServiceImpl;
        this.employeeServiceImpl = employeeServiceImpl;
    }

    public void run() {
        boolean running = true;

        while (running) {
            System.out.println("\nВыберите действие:");
            System.out.println("1. Найти общий вес продукции на заводе");
            System.out.println("2. Найти среднюю цену за продукт");
            System.out.println("3. Вывести имя оптимального продукта (лучшее соотношение цена и вес)");
            System.out.println("4. Записать отчетность по продукции в файл");
            System.out.println("5. Нанять нового работника");
            System.out.println("6. Уволить работника");
            System.out.println("7. Показать список всех работников с привязанными продуктами к ним");
            System.out.println("8. Изменить информацию о сотруднике");
            System.out.println("9. Создать продукт");
            System.out.println("10. Показать список всех продуктов");
            System.out.println("11. Обновить информацию о продукте");
            System.out.println("12. Удалить продукт");
            System.out.println("0. Выйти из программы");

            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    System.out.println("Общий вес продукции на заводе: " + productServiceImpl.calculateTotalWeight());
                    break;
                case 2:
                    System.out.println("Средняя цена за продукт: " + productServiceImpl.calculateAveragePrice());
                    break;
                case 3:
                    System.out.println("Оптимальный продукт: " + productServiceImpl.findOptimalProduct().getName());
                    break;
                case 4:
                    writeReportToFile("report.txt");
                    break;
                case 5:
                    hireEmployee();
                    break;
                case 6:
                    fireEmployee();
                    break;
                case 7:
                    showEmployeeList();
                    break;
                case 8:
                    updateEmployeeInfo();
                    break;
                case 9:
                    createProduct();
                    break;
                case 10:
                    showProductList();
                    break;
                case 11:
                    updateProductInfo();
                    break;
                case 12:
                    deleteProduct();
                    break;
                case 0:
                    System.out.println("Программа завершена.");
                    running = false;
                    break;
                default:
                    System.out.println("Неверный выбор. Попробуйте еще раз.");
            }
        }
        scanner.close();
    }

    public void writeReportToFile(String fileName) {
        try (FileWriter writer = new FileWriter(fileName)) {
            writer.write("Отчетность по продукции на заводе:\n");
            writer.write("-------------------------------\n");
            writer.write("Средняя цена за продукт: " + productServiceImpl.calculateAveragePrice() + "\n");
            writer.write("Общий вес продукции на заводе: " + productServiceImpl.calculateTotalWeight() + "\n");
            writer.write("Оптимальный продукт: " + productServiceImpl.findOptimalProduct().getName() + "\n");
            writer.write("-------------------------------\n");

            writer.write("Сотрудники и их ответственность за продукты:\n");
            for (Employee employee : employeeServiceImpl.getEmployees().values()) {
                writer.write(employee.getName() + " - " + employee.getProduct().getName() + "\n");
            }
            writer.write("-------------------------------\n");
            System.out.println("Отчет успешно записан в файл " + fileName);
        } catch (IOException e) {
            System.out.println("Ошибка при записи отчета в файл.");
            e.printStackTrace();
        }
    }

    public void hireEmployee() {
        System.out.println("Введите имя нового работника:");
        String name = scanner.nextLine();
        System.out.println("Введите продукт, за который будет ответственен новый работник:");
        String productName = scanner.nextLine();
        Product product = productServiceImpl.findProductByName(productName);
        if (product != null) {
            Employee employee = new Employee(name, product);
            employeeServiceImpl.hireEmployee(employee);
            System.out.println("Новый работник " + name + " нанят и ответственен за продукт " + productName);
        } else {
            System.out.println("Продукт с таким названием не найден.");
        }
    }

    public void fireEmployee() {
        System.out.println("Введите имя работника, которого нужно уволить:");
        String name = scanner.nextLine();
        employeeServiceImpl.fireEmployee(name);
        System.out.println("Работник " + name + " уволен.");
    }

    public void showEmployeeList() {
        System.out.println("Список всех работников и их привязанных продуктов:");
        for (Employee employee : employeeServiceImpl.getEmployees().values()) {
            System.out.println(employee.getName() + ": " + employee.getProduct().getName());
        }
    }

    public void updateEmployeeInfo() {
        System.out.println("Введите имя сотрудника, информацию о котором хотите изменить:");
        String name = scanner.nextLine();
        Employee employee = employeeServiceImpl.findEmployeeByName(name);
        if (employee != null) {
            System.out.println("Введите новый продукт, за который будет ответственен сотрудник:");
            String productName = scanner.nextLine();
            Product product = productServiceImpl.findProductByName(productName);
            if (product != null) {
                employee.setProduct(product);
                System.out.println("Информация о сотруднике " + name + " успешно обновлена.");
            } else {
                System.out.println("Продукт с таким названием не найден.");
            }
        } else {
            System.out.println("Сотрудник с таким именем не найден.");
        }
    }

    public void createProduct() {
        System.out.println("Введите название нового продукта:");
        String name = scanner.nextLine();
        System.out.println("Введите вес нового продукта:");
        int weight = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Введите цену нового продукта:");
        double price = scanner.nextDouble();
        scanner.nextLine();

        Product product = new Product(name, weight, price);
        productServiceImpl.addProduct(product);
        System.out.println("Новый продукт успешно создан.");
    }

    public void showProductList() {
        System.out.println("Список всех продуктов:");
        for (Product product : productServiceImpl.getProducts()) {
            System.out.println("Название: " + product.getName() + ", Вес: " + product.getWeight() + ", Цена: " + product.getPrice());
        }
    }

    public void updateProductInfo() {
        System.out.println("Введите название продукта, информацию о котором хотите изменить:");
        String name = scanner.nextLine();
        Product product = productServiceImpl.findProductByName(name);
        if (product != null) {
            System.out.println("Введите новое название для продукта:");
            String newName = scanner.nextLine();
            System.out.println("Введите новый вес для продукта:");
            int newWeight = scanner.nextInt();
            scanner.nextLine();
            System.out.println("Введите новую цену для продукта:");
            double newPrice = scanner.nextDouble();
            scanner.nextLine();

            product.setName(newName);
            product.setWeight(newWeight);
            product.setPrice(newPrice);

            productServiceImpl.updateProduct(product);
            System.out.println("Информация о продукте успешно обновлена.");
        } else {
            System.out.println("Продукт с таким названием не найден.");
        }
    }

    public void deleteProduct() {
        System.out.println("Введите название продукта, который хотите удалить:");
        String name = scanner.nextLine();
        Product product = productServiceImpl.findProductByName(name);
        if (product != null) {
            productServiceImpl.deleteProduct(product);
            System.out.println("Продукт успешно удален.");
        } else {
            System.out.println("Продукт с таким названием не найден.");
        }
    }
}